from tkinter.ttk import Combobox
from tkinter import *


def run():
    params = {
        'rows': 15,
        'columns': 15,
        'generator': 2,
        'draw_steps': True
    }

    def clicked():
        params['rows'] = int(str(rows.get()))
        params['columns'] = int(str(cols.get()))
        params['generator'] = int(str(combo.get()))
        params['draw_steps'] = bool(chk_state.get())
        window.destroy()

    window = Tk()
    window.title("Добро пожаловать в приложение PythonRu")
    window.geometry('400x250')

    Label(window, text="Количество строк(высота)").grid(column=0, row=0)
    rows = Entry(window, width=10)
    rows.grid(column=0, row=1)
    Label(window, text="Количество столбцов(ширина)").grid(column=0, row=2)
    cols = Entry(window, width=10)
    cols.grid(column=0, row=3)
    Label(window, text="Выберите алгоритм генерации").grid(column=0, row=4)
    combo = Combobox(window)
    combo['values'] = (1, 2)
    combo.current(1)  # установите вариант по умолчанию
    combo.grid(column=0, row=5)
    chk_state = BooleanVar()
    chk_state.set(True)  # задайте проверку состояния чекбокса
    draw_steps = Checkbutton(window, text="Рисовать процесс построения?", var=chk_state)
    draw_steps.grid(column=0, row=6)
    btn = Button(window, text="Принять!", command=clicked)
    btn.grid(column=0, row=7)

    window.mainloop()

    return params


if __name__ == '__main__':
    run()
