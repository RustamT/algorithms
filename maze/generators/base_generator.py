from abc import ABC, abstractmethod


class BaseGenerator(ABC):
    @abstractmethod
    def generate_maze(self, draw_steps=False):
        raise NotImplemented()
