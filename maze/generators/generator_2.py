import queue
import random
import sys

import pygame

from .base_generator import BaseGenerator


# algorithm from https://habr.com/ru/post/262345/
class MazeGenerator(BaseGenerator):
    def __init__(self, board):
        """
        matrix: standard maze template, each cell is separated by walls
        """
        self.board = board

    def generate_maze(self, draw_steps=False):
        self.destroyed_walls = []
        self.visited_set = set()
        self.visited = queue.LifoQueue()

        current_cell_number = next(iter(self.board.adjacency_dict))  # left top non wall_cell

        while True:
            self.visited_set.add(current_cell_number)
            next_cell_number = self._get_unvisited_neighbour(current_cell_number)
            if next_cell_number:
                self.visited.put(current_cell_number)
                self._delete_wall_between_cells(current_cell_number, next_cell_number)
                current_cell_number = next_cell_number
            else:
                current_cell_number = self.visited.get()
            if self.visited.empty():
                break
            if draw_steps:
                self.board.draw_board_matrix(selected_cell_number=current_cell_number)
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        sys.exit()
                pygame.display.update()

    def _get_unvisited_neighbour(self, cell_number):
        all_neighbours = self.board.adjacency_dict[cell_number]
        numbers = all_neighbours.values()
        unvisited = []
        for number in numbers:
            if number not in self.visited_set:
                unvisited.append(number)
        if unvisited:
            return random.choice(unvisited)

    def _delete_wall_between_cells(self, cell_number, next_cell_number):
        diff = next_cell_number - cell_number
        cell = None
        if diff == 2:
            cell = self.board.cell_dict[next_cell_number - 1]
        elif diff == -2:
            cell = self.board.cell_dict[next_cell_number + 1]
        elif diff > 2:
            wall_cell_number = next_cell_number - self.board.width
            cell = self.board.cell_dict[wall_cell_number]
        elif diff < 2:
            wall_cell_number = next_cell_number + self.board.width
            cell = self.board.cell_dict[wall_cell_number]

        if cell:
            self.destroyed_walls.append(cell)
            cell.set_type_cell()  # it's not wall anymore
