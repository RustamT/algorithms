import random
import sys
import pygame

from maze.generators.base_generator import BaseGenerator
from maze.elements.board import Board
from maze.elements.cell import Cell


# unknown algorithm. Just copy-pasted from web
class MazeGenerator(BaseGenerator):
    def __init__(self, board: Board):
        self.board = board
        self.height = self.board.height
        self.width = self.board.width
        self.board.matrix = []

    def _surrounding_cells(self, rand_wall):
        s_cells = 0
        if self.board.matrix[rand_wall[0] - 1][rand_wall[1]].status == 'c':
            s_cells += 1
        if self.board.matrix[rand_wall[0] + 1][rand_wall[1]].status == 'c':
            s_cells += 1
        if self.board.matrix[rand_wall[0]][rand_wall[1] - 1].status == 'c':
            s_cells += 1
        if self.board.matrix[rand_wall[0]][rand_wall[1] + 1].status == 'c':
            s_cells += 1

        return s_cells

    def generate_maze(self, draw_steps=False):

        cell_number = 1
        for row_number in range(self.height):
            y = self.board.cell_height * row_number
            new_row = []
            for column_number in range(self.width):
                x = self.board.cell_width * column_number
                cell = Cell(x, y, self.board.cell_width,
                            self.board.cell_height, number=cell_number)
                cell.status = 'u'
                cell_number += 1
                new_row.append(cell)
                self.board.cell_dict[cell.number] = cell
            self.board.matrix.append(new_row)

        # Randomize starting point and set it a cell
        starting_height = int(random.random() * self.height)
        starting_width = int(random.random() * self.width)
        if starting_height == 0:
            starting_height += 1
        if starting_height == self.height - 1:
            starting_height -= 1
        if starting_width == 0:
            starting_width += 1
        if starting_width == self.width - 1:
            starting_width -= 1

        # Mark it as cell and add surrounding walls to the list
        self.board.matrix[starting_height][starting_width].status = 'c'
        walls = [[starting_height - 1, starting_width], [starting_height, starting_width - 1],
                 [starting_height, starting_width + 1], [starting_height + 1, starting_width]]

        # Denote walls in matrix
        self.board.matrix[starting_height - 1][starting_width].status = 'w'
        self.board.matrix[starting_height][starting_width - 1].status = 'w'
        self.board.matrix[starting_height][starting_width + 1].status = 'w'
        self.board.matrix[starting_height + 1][starting_width].status = 'w'

        while walls:
            if draw_steps:
                if draw_steps:
                    self.board.draw_board_matrix()
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            sys.exit()
                    pygame.display.update()

            # Pick a random wall
            rand_wall = walls[int(random.random() * len(walls)) - 1]
            # Check if it is a left wall
            if rand_wall[1] != 0:
                if (
                    self.board.matrix[rand_wall[0]][rand_wall[1] - 1].status == 'u' and
                    self.board.matrix[rand_wall[0]][rand_wall[1] + 1].status == 'c'
                ):
                    # Find the number of surrounding cells
                    s_cells = self._surrounding_cells(rand_wall)

                    if s_cells < 2:
                        # Denote the new path
                        self.board.matrix[rand_wall[0]][rand_wall[1]].status = 'c'
                        self.board.matrix[rand_wall[0]][rand_wall[1]].set_type_cell()

                        # Mark the new walls
                        # Upper cell
                        if rand_wall[0] != 0:
                            if self.board.matrix[rand_wall[0] - 1][rand_wall[1]].status != 'c':
                                self.board.matrix[rand_wall[0] - 1][rand_wall[1]].status = 'w'
                                self.board.matrix[rand_wall[0] - 1][rand_wall[1]].set_type_wall()
                            if [rand_wall[0] - 1, rand_wall[1]] not in walls:
                                walls.append([rand_wall[0] - 1, rand_wall[1]])

                        # Bottom cell
                        if rand_wall[0] != self.height - 1:
                            if self.board.matrix[rand_wall[0] + 1][rand_wall[1]].status != 'c':
                                self.board.matrix[rand_wall[0] + 1][rand_wall[1]].status = 'w'
                                self.board.matrix[rand_wall[0] + 1][rand_wall[1]].set_type_wall()
                            if [rand_wall[0] + 1, rand_wall[1]] not in walls:
                                walls.append([rand_wall[0] + 1, rand_wall[1]])

                        # Leftmost cell
                        if rand_wall[1] != 0:
                            if self.board.matrix[rand_wall[0]][rand_wall[1] - 1].status != 'c':
                                self.board.matrix[rand_wall[0]][rand_wall[1] - 1].status = 'w'
                                self.board.matrix[rand_wall[0]][rand_wall[1] - 1].set_type_wall()
                            if [rand_wall[0], rand_wall[1] - 1] not in walls:
                                walls.append([rand_wall[0], rand_wall[1] - 1])

                    # Delete wall
                    for wall in walls:
                        if wall[0] == rand_wall[0] and wall[1] == rand_wall[1]:
                            walls.remove(wall)

                    continue

            # Check if it is an upper wall
            if rand_wall[0] != 0:
                if (self.board.matrix[rand_wall[0] - 1][rand_wall[1]].status == 'u' and
                    self.board.matrix[rand_wall[0] + 1][
                        rand_wall[1]].status == 'c'):

                    s_cells = self._surrounding_cells(rand_wall)
                    if s_cells < 2:
                        # Denote the new path
                        self.board.matrix[rand_wall[0]][rand_wall[1]].status = 'c'
                        self.board.matrix[rand_wall[0]][rand_wall[1]].set_type_cell()

                        # Mark the new walls
                        # Upper cell
                        if rand_wall[0] != 0:
                            if self.board.matrix[rand_wall[0] - 1][rand_wall[1]].status != 'c':
                                self.board.matrix[rand_wall[0] - 1][rand_wall[1]].status = 'w'
                                self.board.matrix[rand_wall[0] - 1][rand_wall[1]].set_type_wall()
                            if [rand_wall[0] - 1, rand_wall[1]] not in walls:
                                walls.append([rand_wall[0] - 1, rand_wall[1]])

                        # Leftmost cell
                        if rand_wall[1] != 0:
                            if self.board.matrix[rand_wall[0]][rand_wall[1] - 1].status != 'c':
                                self.board.matrix[rand_wall[0]][rand_wall[1] - 1].status = 'w'
                                self.board.matrix[rand_wall[0]][rand_wall[1] - 1].set_type_wall()
                            if [rand_wall[0], rand_wall[1] - 1] not in walls:
                                walls.append([rand_wall[0], rand_wall[1] - 1])

                        # Rightmost cell
                        if rand_wall[1] != self.width - 1:
                            if self.board.matrix[rand_wall[0]][rand_wall[1] + 1].status != 'c':
                                self.board.matrix[rand_wall[0]][rand_wall[1] + 1].status = 'w'
                                self.board.matrix[rand_wall[0]][rand_wall[1] + 1].set_type_wall()
                            if [rand_wall[0], rand_wall[1] + 1] not in walls:
                                walls.append([rand_wall[0], rand_wall[1] + 1])

                    # Delete wall
                    for wall in walls:
                        if wall[0] == rand_wall[0] and wall[1] == rand_wall[1]:
                            walls.remove(wall)

                    continue

            # Check the bottom wall
            if rand_wall[0] != self.height - 1:
                if (self.board.matrix[rand_wall[0] + 1][rand_wall[1]].status == 'u' and
                    self.board.matrix[rand_wall[0] - 1][
                        rand_wall[1]].status == 'c'):

                    s_cells = self._surrounding_cells(rand_wall)
                    if s_cells < 2:
                        # Denote the new path
                        self.board.matrix[rand_wall[0]][rand_wall[1]].status = 'c'
                        self.board.matrix[rand_wall[0]][rand_wall[1]].set_type_cell()

                        # Mark the new walls
                        if rand_wall[0] != self.height - 1:
                            if self.board.matrix[rand_wall[0] + 1][rand_wall[1]].status != 'c':
                                self.board.matrix[rand_wall[0] + 1][rand_wall[1]].status = 'w'
                                self.board.matrix[rand_wall[0] + 1][rand_wall[1]].set_type_wall()
                            if [rand_wall[0] + 1, rand_wall[1]] not in walls:
                                walls.append([rand_wall[0] + 1, rand_wall[1]])
                        if rand_wall[1] != 0:
                            if self.board.matrix[rand_wall[0]][rand_wall[1] - 1].status != 'c':
                                self.board.matrix[rand_wall[0]][rand_wall[1] - 1].status = 'w'
                                self.board.matrix[rand_wall[0]][rand_wall[1] - 1].set_type_wall()
                            if [rand_wall[0], rand_wall[1] - 1] not in walls:
                                walls.append([rand_wall[0], rand_wall[1] - 1])
                        if rand_wall[1] != self.width - 1:
                            if self.board.matrix[rand_wall[0]][rand_wall[1] + 1].status != 'c':
                                self.board.matrix[rand_wall[0]][rand_wall[1] + 1].status = 'w'
                                self.board.matrix[rand_wall[0]][rand_wall[1] + 1].set_type_wall()
                            if [rand_wall[0], rand_wall[1] + 1] not in walls:
                                walls.append([rand_wall[0], rand_wall[1] + 1])

                    # Delete wall
                    for wall in walls:
                        if wall[0] == rand_wall[0] and wall[1] == rand_wall[1]:
                            walls.remove(wall)

                    continue

            # Check the right wall
            if rand_wall[1] != self.width - 1:
                if (
                    self.board.matrix[rand_wall[0]][rand_wall[1] + 1].status == 'u' and
                    self.board.matrix[rand_wall[0]][rand_wall[1] - 1].status == 'c'
                ):

                    s_cells = self._surrounding_cells(rand_wall)
                    if s_cells < 2:
                        # Denote the new path
                        self.board.matrix[rand_wall[0]][rand_wall[1]].status = 'c'
                        self.board.matrix[rand_wall[0]][rand_wall[1]].set_type_cell()

                        # Mark the new walls
                        if rand_wall[1] != self.width - 1:
                            if self.board.matrix[rand_wall[0]][rand_wall[1] + 1].status != 'c':
                                self.board.matrix[rand_wall[0]][rand_wall[1] + 1].status = 'w'
                                self.board.matrix[rand_wall[0]][rand_wall[1] + 1].set_type_wall()
                            if [rand_wall[0], rand_wall[1] + 1] not in walls:
                                walls.append([rand_wall[0], rand_wall[1] + 1])
                        if rand_wall[0] != self.height - 1:
                            if self.board.matrix[rand_wall[0] + 1][rand_wall[1]].status != 'c':
                                self.board.matrix[rand_wall[0] + 1][rand_wall[1]].status = 'w'
                                self.board.matrix[rand_wall[0] + 1][rand_wall[1]].set_type_wall()
                            if [rand_wall[0] + 1, rand_wall[1]] not in walls:
                                walls.append([rand_wall[0] + 1, rand_wall[1]])
                        if rand_wall[0] != 0:
                            if self.board.matrix[rand_wall[0] - 1][rand_wall[1]].status != 'c':
                                self.board.matrix[rand_wall[0] - 1][rand_wall[1]].status = 'w'
                                self.board.matrix[rand_wall[0] - 1][rand_wall[1]].set_type_wall()
                            if [rand_wall[0] - 1, rand_wall[1]] not in walls:
                                walls.append([rand_wall[0] - 1, rand_wall[1]])

                    # Delete wall
                    for wall in walls:
                        if wall[0] == rand_wall[0] and wall[1] == rand_wall[1]:
                            walls.remove(wall)

                    continue

            # Delete the wall from the list anyway
            for wall in walls:
                if wall[0] == rand_wall[0] and wall[1] == rand_wall[1]:
                    walls.remove(wall)

        # Mark the remaining unvisited cells as walls
        for i in range(0, self.height):
            for j in range(0, self.width):
                if self.board.matrix[i][j].status == 'u':
                    self.board.matrix[i][j].status = 'w'
                    self.board.matrix[i][j].set_type_wall()

        # Set entrance and exit
        for i in range(0, self.width):
            if self.board.matrix[1][i].status == 'c':
                self.board.matrix[0][i].status = 'c'
                self.board.matrix[0][i].set_type_cell()
                break

        for i in range(self.width - 1, 0, -1):
            if self.board.matrix[self.height - 2][i].status == 'c':
                self.board.matrix[self.height - 1][i].status = 'c'
                self.board.matrix[self.height - 1][i].set_type_cell()
                break

    def _set_wall_types(self):
        """
            matrix: nested list where each list is row containing 'c' as cell and 'w' as wall.
            example: [[w, w, c], [c, c, c], [w, c, w]]
            """
        for row_number, row in enumerate(self.board.matrix):
            for column_number, cell in enumerate(row):
                if cell.status == 'w':
                    cell.set_type_wall()


