from collections import OrderedDict
from math import ceil

from pygame import Surface
import pygame

from .structual_elements import MyColor
from .cell import Cell


class Board:
    """
    Object stores matrix of cells
    """
    rect_border_width = 5
    wall_color = MyColor.black
    cell_color = MyColor.antiquewhite

    def __init__(self, number_of_rows: int, number_of_columns: int, cell_size: (int, int)):
        self.height = number_of_rows
        self.width = number_of_columns
        self.cell_width = cell_size[0]
        self.cell_height = cell_size[1]
        self.width_in_px = self._calculate_board_width()
        self.height_in_px = self._calculate_board_height()
        self.adjacency_dict = OrderedDict()
        self.cell_dict = {}  # access to cells in matrix by its number

    def set_screen(self, screen: Surface):
        """
        :param screen: surface on which our board will be drawn
        """
        self.screen = screen
        assert self.height_in_px <= self.screen.get_height()
        assert self.width_in_px <= self.screen.get_width()

    def set_matrix(self, matrix):
        self.matrix = matrix

    def draw_board_matrix(self, draw_cell_number=False, selected_cell_number=None):
        for column in self.matrix:
            for cell in column:
                cell.color = cell.wall_color if cell.is_wall else cell.cell_color
                if selected_cell_number and selected_cell_number == cell.number:
                    cell.color = MyColor.green4

                rect = pygame.draw.rect(self.screen, cell.color,
                                        [cell.x, cell.y, cell.width, cell.height], 0)
                # black border around rect
                pygame.draw.rect(self.screen, MyColor.black,
                                 [cell.x, cell.y, cell.width, cell.height], 2)

                if draw_cell_number:
                    text_surface_object = pygame.font.SysFont(
                        'Arial', 11).render(str(cell.number), True, MyColor.red)
                    text_rect = text_surface_object.get_rect(center=rect.center)
                    self.screen.blit(text_surface_object, text_rect)

    def generate_straight_matrix(self):
        """
        creates a matrix with cells surrounded by walls
        """
        self.matrix = []
        is_wall_row = True
        is_wall_column = True
        cell_number = 1
        for row_number in range(self.height):
            y = self.cell_height * row_number
            new_row = []
            if row_number == 0:
                is_wall_row = True

            for column_number in range(self.width):
                x = self.cell_width * column_number
                cell = Cell(x, y, self.cell_width, self.cell_height, number=cell_number)
                if column_number == 0:
                    is_wall_column = True
                if is_wall_row:
                    cell.set_type_wall()
                if is_wall_column:
                    cell.set_type_wall()

                is_wall_column = not is_wall_column
                cell_number += 1
                new_row.append(cell)
                self._add_to_adjacency_dict(cell)
                self.cell_dict[cell.number] = cell

            is_wall_row = not is_wall_row
            self.matrix.append(new_row)

    def _add_to_adjacency_dict(self, cell):
        """
        Словарь с репрезентацией лабиринта в виде {клетка:[соседние клетки через стенку]}.
        """
        if cell.is_wall:
            return
        cell_number = cell.number

        right_neighbour_row = ceil((cell_number + 2) / self.width)
        right_neighbour = cell_number + 2 if cell.get_row() == right_neighbour_row else None

        left_neighbour_row = ceil((cell_number - 2) / self.width)
        left_neighbour = cell_number - 2 if cell.get_row() == left_neighbour_row else None

        bottom_neighbour = 2 * self.width + cell_number
        b_neighbour_row = ceil(bottom_neighbour / self.width)
        bottom_neighbour = bottom_neighbour if b_neighbour_row < self.height else None

        top_neighbour = cell_number - 2 * self.width
        top_neighbour = top_neighbour if top_neighbour > 0 else None

        self.adjacency_dict[cell_number] = {}
        if right_neighbour:
            self.adjacency_dict[cell_number].update({'right': right_neighbour})
        if left_neighbour:
            self.adjacency_dict[cell_number].update({'left': left_neighbour})
        if top_neighbour:
            self.adjacency_dict[cell_number].update({'top': top_neighbour})
        if bottom_neighbour:
            self.adjacency_dict[cell_number].update({'bottom': bottom_neighbour})

    def print_board(self):
        # prints board to shell.
        """
        Black        0;30     Dark Gray     1;30
        Red          0;31     Light Red     1;31
        Green        0;32     Light Green   1;32
        Brown/Orange 0;33     Yellow        1;33
        Blue         0;34     Light Blue    1;34
        Purple       0;35     Light Purple  1;35
        Cyan         0;36     Light Cyan    1;36
        Light Gray   0;37     White         1;37
        """
        def set_color_for_print(cell):
            red = '\033[0;31m'
            green = '\033[0;32m'
            nc = '\033[0m'  # No Color
            number = str(cell.number)
            while len(number) < 4:
                number += ' '

            return f'{red}{number}{nc}' if cell.is_wall else f'{green}{number}{nc}'

        print_collection = []
        for row in self.matrix:
            print_collection.append('|'.join(list(map(set_color_for_print, row))))
        [print(row) for row in print_collection]

    def _calculate_board_width(self):
        return self.width * self.cell_width

    def _calculate_board_height(self):
        return self.height * self.cell_height
