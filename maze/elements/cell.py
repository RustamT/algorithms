from .structual_elements import MyColor


class Cell:
    cell_color = MyColor.antiquewhite
    wall_color = MyColor.black

    def __init__(self, x, y, w=10, h=10, number: int = 0, color=None, is_wall=False):
        self.x = x
        self.y = y
        self.height = h
        self.width = w
        self.is_wall = is_wall
        self.number = number  # required to build graph
        if color:
            self._color = color

    @property
    def color(self):
        if hasattr(self, '_color'):
            return self._color

        if self.is_wall:
            return self.wall_color
        else:
            return self.cell_color

    @color.setter
    def color(self, color):
        self._color = color

    def set_type_wall(self):
        self.is_wall = True
        self._color = self.wall_color

    def set_type_cell(self):
        self.is_wall = False
        self._color = self.cell_color

    def get_column(self):
        return self.x // self.width + 1

    def get_row(self):
        return self.y // self.height + 1

    def __repr__(self):
        return f" Cell(column:{self.get_column()}, row:{self.get_row()}," \
               f" color:{self.color})"
