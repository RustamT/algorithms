import pygame
import sys

from maze import welcome_screen
from elements import Board
from elements.structual_elements import MyColor
from generators import generator_1, generator_2


class Director:
    generators = {
        1: generator_1.MazeGenerator,
        2: generator_2.MazeGenerator
    }

    def __init__(self, nrof_rows: int, nrof_columns: int):
        self._height = nrof_rows
        self._width = nrof_columns
        self.board = self._get_start_board()

    def prepare_scene(self, generator_type: int, draw_steps=False):
        main_screen = pygame.display.set_mode((self.board.width_in_px, self.board.height_in_px),
                                              0, 32)
        self.board.set_screen(main_screen)
        self.board.screen.fill(MyColor.whitesmoke)
        self.generators.get(generator_type)(self.board).generate_maze(draw_steps=draw_steps)

    def _get_start_board(self):
        cell_width = 20
        cell_height = 15

        board = Board(self._height, self._width, cell_size=(cell_width, cell_height))
        board.generate_straight_matrix()

        return board


def main():
    pygame.init()
    params = welcome_screen.run()
    director = Director(params['rows'], params['columns'])

    director.prepare_scene(params['generator'], params['draw_steps'])

    director.board.print_board()
    director.board.draw_board_matrix()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()


if __name__ == '__main__':
    main()
